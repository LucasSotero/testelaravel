<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moeda extends Model
{
    protected $fillable = [
        'name', 'symbol', 'marketCap', 'price', 'volume', 'circulating', 'change'
    ];
}
