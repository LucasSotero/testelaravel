<?php

namespace App\Http\Controllers;

use App\Moeda;
use Illuminate\Http\Request;

class MoedaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moedas = Moeda::paginate(10);
        // dd($moedas);
        return view('home', compact('moedas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        
        foreach ($request->data as $moeda) {
            $m = new Moeda();
            // Moeda::create([
            //     'name' => $moeda['name']
            // ])
            $m->name = $moeda['name'];
            $m->symbol = $moeda['symbol'];
            $m->marketCap = $moeda['quotes']['USD']['market_cap'];   
            $m->price = $moeda['quotes']['USD']['price'];
            $m->volume = $moeda['quotes']['USD']['volume_24h']; 
            $m->circulating = $moeda['circulating_supply'];
            $m->change = $moeda['quotes']['USD']['percent_change_24h'];   
            $m->save();
        }
        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Moeda  $moeda
     * @return \Illuminate\Http\Response
     */
    public function show(Moeda $moeda)
    {
        return $moeda;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Moeda  $moeda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Moeda $moeda)
    {
        $moeda->update($request->all());
        return $moeda;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Moeda  $moeda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Moeda $moeda)
    {
        $moeda->delete();
        return $moeda;
    }
}
