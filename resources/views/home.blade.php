@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Symbol</th>
                <th>Market cap</th>
                <th>Price</th>
                <th>Volume</th>
                <th>Circulary</th>
                <th>change</th>
            </tr>
        </thead>
        <tbody>
           
            @foreach ($moedas as $moeda)
                <tr>
                    <td>{{ $moeda->id }}</td>
                    <td>{{ $moeda->name }}</td>
                    <td>{{ $moeda->symbol }}</td>
                    <td>{{ $moeda->marketCap }}</td>
                    <td>{{ $moeda->price }}</td>
                    <td>{{ $moeda->volume }}</td>
                    <td>{{ $moeda->circulating }}</td>
                    <td>{{ $moeda->change }}</td>
                </tr>
            @endforeach
            
        </tbody>
    </table>
    {!! $moedas->links() !!}
</div>


@endsection

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> --}}