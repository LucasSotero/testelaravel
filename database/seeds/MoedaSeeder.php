<?php

use Illuminate\Database\Seeder;

class MoedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Moeda::create([
            'name' => 'teste',
            'symbol' => 't',
            'marketCap' => 10,
            'price' => 1000,
            'volume' => 10,
            'circulating' => 10,
            'change' => 10
        ]);
    }
}
